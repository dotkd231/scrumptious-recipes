from django.contrib import admin
from .models import FoodItem, Recipe, Measure, Ingredient, Step

admin.site.register([Recipe, Measure, FoodItem, Ingredient, Step])
